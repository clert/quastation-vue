package com.quastatioin.xtp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.quastatioin.common.annotation.Excel;
import com.quastatioin.common.core.domain.BaseEntity;

/**
 * 策略信息对象 xtp_game_info
 *
 * @author CCLERT
 * @date 2024-07-09
 */
public class XtpGameInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long gameId;

    /** 策略名称 */
    @Excel(name = "策略名称")
    private String gameName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long createUser;

    /** 策略文件路径 */
    @Excel(name = "策略文件路径")
    private String gameFileUrl;

    /** 策略MD5值 */
    @Excel(name = "策略MD5值")
    private String gameFileMd5;

    public void setGameId(Long gameId)
    {
        this.gameId = gameId;
    }

    public Long getGameId()
    {
        return gameId;
    }
    public void setGameName(String gameName)
    {
        this.gameName = gameName;
    }

    public String getGameName()
    {
        return gameName;
    }
    public void setCreateUser(Long createUser)
    {
        this.createUser = createUser;
    }

    public Long getCreateUser()
    {
        return createUser;
    }
    public void setGameFileUrl(String gameFileUrl)
    {
        this.gameFileUrl = gameFileUrl;
    }

    public String getGameFileUrl()
    {
        return gameFileUrl;
    }
    public void setGameFileMd5(String gameFileMd5)
    {
        this.gameFileMd5 = gameFileMd5;
    }

    public String getGameFileMd5()
    {
        return gameFileMd5;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("gameId", getGameId())
                .append("gameName", getGameName())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("createUser", getCreateUser())
                .append("updateTime", getUpdateTime())
                .append("updateBy", getUpdateBy())
                .append("remark", getRemark())
                .append("gameFileUrl", getGameFileUrl())
                .append("gameFileMd5", getGameFileMd5())
                .toString();
    }
}
