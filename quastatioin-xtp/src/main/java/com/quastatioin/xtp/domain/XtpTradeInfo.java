package com.quastatioin.xtp.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.quastatioin.common.annotation.Excel;
import com.quastatioin.common.core.domain.BaseEntity;

/**
 * 交易记录对象 xtp_trade_info
 *
 * @author CCCLERT
 * @date 2024-07-09
 */
public class XtpTradeInfo
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long tradeId;

    /** 订单ID */
    @Excel(name = "订单ID")
    private String orderXtpId;

    /** 客户端ID */
    @Excel(name = "客户端ID")
    private String orderClientId;

    /** 市场来源 */
    @Excel(name = "市场来源")
    private String market;

    /** 股票代码 */
    @Excel(name = "股票代码")
    private String ticker;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 成交量 */
    @Excel(name = "成交量")
    private String quantity;

    /** 价格类型 */
    @Excel(name = "价格类型")
    private String priceType;

    /** 提交时间 */
    @Excel(name = "提交时间")
    private String insertTime;

    /** 成交价钱 */
    @Excel(name = "成交价钱")
    private String tradeAmount;

    /** 更新时间 */
    private String updateTime;

    public void setTradeId(Long tradeId)
    {
        this.tradeId = tradeId;
    }

    public Long getTradeId()
    {
        return tradeId;
    }
    public void setOrderXtpId(String orderXtpId)
    {
        this.orderXtpId = orderXtpId;
    }

    public String getOrderXtpId()
    {
        return orderXtpId;
    }
    public void setOrderClientId(String orderClientId)
    {
        this.orderClientId = orderClientId;
    }

    public String getOrderClientId()
    {
        return orderClientId;
    }
    public void setMarket(String market)
    {
        this.market = market;
    }

    public String getMarket()
    {
        return market;
    }
    public void setTicker(String ticker)
    {
        this.ticker = ticker;
    }

    public String getTicker()
    {
        return ticker;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getQuantity()
    {
        return quantity;
    }
    public void setPriceType(String priceType)
    {
        this.priceType = priceType;
    }

    public String getPriceType()
    {
        return priceType;
    }
    public void setInsertTime(String insertTime)
    {
        this.insertTime = insertTime;
    }

    public String getInsertTime()
    {
        return insertTime;
    }
    public void setTradeAmount(String tradeAmount)
    {
        this.tradeAmount = tradeAmount;
    }

    public String getTradeAmount()
    {
        return tradeAmount;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("tradeId", getTradeId())
                .append("orderXtpId", getOrderXtpId())
                .append("orderClientId", getOrderClientId())
                .append("market", getMarket())
                .append("ticker", getTicker())
                .append("price", getPrice())
                .append("quantity", getQuantity())
                .append("priceType", getPriceType())
                .append("insertTime", getInsertTime())
                .append("tradeAmount", getTradeAmount())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
