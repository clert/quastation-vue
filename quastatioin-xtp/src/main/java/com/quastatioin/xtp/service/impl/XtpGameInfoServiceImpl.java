package com.quastatioin.xtp.service.impl;

import java.util.List;
import com.quastatioin.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.quastatioin.xtp.mapper.XtpGameInfoMapper;
import com.quastatioin.xtp.domain.XtpGameInfo;
import com.quastatioin.xtp.service.IXtpGameInfoService;

/**
 * 策略信息Service业务层处理
 *
 * @author CCLERT
 * @date 2024-07-09
 */
@Service
public class XtpGameInfoServiceImpl implements IXtpGameInfoService
{
    @Autowired
    private XtpGameInfoMapper xtpGameInfoMapper;

    /**
     * 查询策略信息
     *
     * @param gameId 策略信息主键
     * @return 策略信息
     */
    @Override
    public XtpGameInfo selectXtpGameInfoByGameId(Long gameId)
    {
        return xtpGameInfoMapper.selectXtpGameInfoByGameId(gameId);
    }

    /**
     * 查询策略信息列表
     *
     * @param xtpGameInfo 策略信息
     * @return 策略信息
     */
    @Override
    public List<XtpGameInfo> selectXtpGameInfoList(XtpGameInfo xtpGameInfo)
    {
        return xtpGameInfoMapper.selectXtpGameInfoList(xtpGameInfo);
    }

    /**
     * 新增策略信息
     *
     * @param xtpGameInfo 策略信息
     * @return 结果
     */
    @Override
    public int insertXtpGameInfo(XtpGameInfo xtpGameInfo)
    {
        xtpGameInfo.setCreateTime(DateUtils.getNowDate());
        return xtpGameInfoMapper.insertXtpGameInfo(xtpGameInfo);
    }

    /**
     * 修改策略信息
     *
     * @param xtpGameInfo 策略信息
     * @return 结果
     */
    @Override
    public int updateXtpGameInfo(XtpGameInfo xtpGameInfo)
    {
        xtpGameInfo.setUpdateTime(DateUtils.getNowDate());
        return xtpGameInfoMapper.updateXtpGameInfo(xtpGameInfo);
    }

    /**
     * 批量删除策略信息
     *
     * @param gameIds 需要删除的策略信息主键
     * @return 结果
     */
    @Override
    public int deleteXtpGameInfoByGameIds(Long[] gameIds)
    {
        return xtpGameInfoMapper.deleteXtpGameInfoByGameIds(gameIds);
    }

    /**
     * 删除策略信息信息
     *
     * @param gameId 策略信息主键
     * @return 结果
     */
    @Override
    public int deleteXtpGameInfoByGameId(Long gameId)
    {
        return xtpGameInfoMapper.deleteXtpGameInfoByGameId(gameId);
    }
}
