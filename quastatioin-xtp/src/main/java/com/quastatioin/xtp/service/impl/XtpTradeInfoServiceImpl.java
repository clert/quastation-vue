package com.quastatioin.xtp.service.impl;

import java.util.List;
import com.quastatioin.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.quastatioin.xtp.mapper.XtpTradeInfoMapper;
import com.quastatioin.xtp.domain.XtpTradeInfo;
import com.quastatioin.xtp.service.IXtpTradeInfoService;

/**
 * 交易记录Service业务层处理
 *
 * @author CCCLERT
 * @date 2024-07-09
 */
@Service
public class XtpTradeInfoServiceImpl implements IXtpTradeInfoService
{
    @Autowired
    private XtpTradeInfoMapper xtpTradeInfoMapper;

    /**
     * 查询交易记录
     *
     * @param tradeId 交易记录主键
     * @return 交易记录
     */
    @Override
    public XtpTradeInfo selectXtpTradeInfoByTradeId(Long tradeId)
    {
        return xtpTradeInfoMapper.selectXtpTradeInfoByTradeId(tradeId);
    }

    /**
     * 查询交易记录列表
     *
     * @param xtpTradeInfo 交易记录
     * @return 交易记录
     */
    @Override
    public List<XtpTradeInfo> selectXtpTradeInfoList(XtpTradeInfo xtpTradeInfo)
    {
        return xtpTradeInfoMapper.selectXtpTradeInfoList(xtpTradeInfo);
    }

    /**
     * 新增交易记录
     *
     * @param xtpTradeInfo 交易记录
     * @return 结果
     */
    @Override
    public int insertXtpTradeInfo(XtpTradeInfo xtpTradeInfo)
    {
        return xtpTradeInfoMapper.insertXtpTradeInfo(xtpTradeInfo);
    }

    /**
     * 修改交易记录
     *
     * @param xtpTradeInfo 交易记录
     * @return 结果
     */
    @Override
    public int updateXtpTradeInfo(XtpTradeInfo xtpTradeInfo)
    {
        return xtpTradeInfoMapper.updateXtpTradeInfo(xtpTradeInfo);
    }

    /**
     * 批量删除交易记录
     *
     * @param tradeIds 需要删除的交易记录主键
     * @return 结果
     */
    @Override
    public int deleteXtpTradeInfoByTradeIds(Long[] tradeIds)
    {
        return xtpTradeInfoMapper.deleteXtpTradeInfoByTradeIds(tradeIds);
    }

    /**
     * 删除交易记录信息
     *
     * @param tradeId 交易记录主键
     * @return 结果
     */
    @Override
    public int deleteXtpTradeInfoByTradeId(Long tradeId)
    {
        return xtpTradeInfoMapper.deleteXtpTradeInfoByTradeId(tradeId);
    }
}
