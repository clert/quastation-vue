package com.quastatioin.xtp.service;

import java.util.List;
import com.quastatioin.xtp.domain.XtpGameInfo;

/**
 * 策略信息Service接口
 *
 * @author CCLERT
 * @date 2024-07-09
 */
public interface IXtpGameInfoService
{
    /**
     * 查询策略信息
     *
     * @param gameId 策略信息主键
     * @return 策略信息
     */
    public XtpGameInfo selectXtpGameInfoByGameId(Long gameId);

    /**
     * 查询策略信息列表
     *
     * @param xtpGameInfo 策略信息
     * @return 策略信息集合
     */
    public List<XtpGameInfo> selectXtpGameInfoList(XtpGameInfo xtpGameInfo);

    /**
     * 新增策略信息
     *
     * @param xtpGameInfo 策略信息
     * @return 结果
     */
    public int insertXtpGameInfo(XtpGameInfo xtpGameInfo);

    /**
     * 修改策略信息
     *
     * @param xtpGameInfo 策略信息
     * @return 结果
     */
    public int updateXtpGameInfo(XtpGameInfo xtpGameInfo);

    /**
     * 批量删除策略信息
     *
     * @param gameIds 需要删除的策略信息主键集合
     * @return 结果
     */
    public int deleteXtpGameInfoByGameIds(Long[] gameIds);

    /**
     * 删除策略信息信息
     *
     * @param gameId 策略信息主键
     * @return 结果
     */
    public int deleteXtpGameInfoByGameId(Long gameId);
}
