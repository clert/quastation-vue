package com.quastatioin.xtp.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.quastatioin.common.annotation.Log;
import com.quastatioin.common.core.controller.BaseController;
import com.quastatioin.common.core.domain.AjaxResult;
import com.quastatioin.common.enums.BusinessType;
import com.quastatioin.xtp.domain.XtpTradeInfo;
import com.quastatioin.xtp.service.IXtpTradeInfoService;
import com.quastatioin.common.utils.poi.ExcelUtil;
import com.quastatioin.common.core.page.TableDataInfo;

/**
 * 交易记录Controller
 *
 * @author CCCLERT
 * @date 2024-07-09
 */
@RestController
@RequestMapping("/xtp/xtpTradeInfo")
public class XtpTradeInfoController extends BaseController
{
    @Autowired
    private IXtpTradeInfoService xtpTradeInfoService;

    /**
     * 查询交易记录列表
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpTradeInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(XtpTradeInfo xtpTradeInfo)
    {
        startPage();
        List<XtpTradeInfo> list = xtpTradeInfoService.selectXtpTradeInfoList(xtpTradeInfo);
        return getDataTable(list);
    }

    /**
     * 导出交易记录列表
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpTradeInfo:export')")
    @Log(title = "交易记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, XtpTradeInfo xtpTradeInfo)
    {
        List<XtpTradeInfo> list = xtpTradeInfoService.selectXtpTradeInfoList(xtpTradeInfo);
        ExcelUtil<XtpTradeInfo> util = new ExcelUtil<XtpTradeInfo>(XtpTradeInfo.class);
        util.exportExcel(response, list, "交易记录数据");
    }

    /**
     * 获取交易记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpTradeInfo:query')")
    @GetMapping(value = "/{tradeId}")
    public AjaxResult getInfo(@PathVariable("tradeId") Long tradeId)
    {
        return success(xtpTradeInfoService.selectXtpTradeInfoByTradeId(tradeId));
    }

    /**
     * 新增交易记录
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpTradeInfo:add')")
    @Log(title = "交易记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody XtpTradeInfo xtpTradeInfo)
    {
        return toAjax(xtpTradeInfoService.insertXtpTradeInfo(xtpTradeInfo));
    }

    /**
     * 修改交易记录
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpTradeInfo:edit')")
    @Log(title = "交易记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody XtpTradeInfo xtpTradeInfo)
    {
        return toAjax(xtpTradeInfoService.updateXtpTradeInfo(xtpTradeInfo));
    }

    /**
     * 删除交易记录
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpTradeInfo:remove')")
    @Log(title = "交易记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tradeIds}")
    public AjaxResult remove(@PathVariable Long[] tradeIds)
    {
        return toAjax(xtpTradeInfoService.deleteXtpTradeInfoByTradeIds(tradeIds));
    }
}
