package com.quastatioin.xtp.controller;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.quastatioin.common.annotation.Log;
import com.quastatioin.common.core.controller.BaseController;
import com.quastatioin.common.core.domain.AjaxResult;
import com.quastatioin.common.enums.BusinessType;
import com.quastatioin.xtp.domain.XtpGameInfo;
import com.quastatioin.xtp.service.IXtpGameInfoService;
import com.quastatioin.common.utils.poi.ExcelUtil;
import com.quastatioin.common.core.page.TableDataInfo;

/**
 * 策略信息Controller
 *
 * @author CCLERT
 * @date 2024-07-09
 */
@RestController
@RequestMapping("/xtp/xtpGameInfo")
public class XtpGameInfoController extends BaseController
{
    @Autowired
    private IXtpGameInfoService xtpGameInfoService;

    /**
     * 查询策略信息列表
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpGameInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(XtpGameInfo xtpGameInfo)
    {
        startPage();
        List<XtpGameInfo> list = xtpGameInfoService.selectXtpGameInfoList(xtpGameInfo);
        return getDataTable(list);
    }

    /**
     * 导出策略信息列表
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpGameInfo:export')")
    @Log(title = "策略信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, XtpGameInfo xtpGameInfo)
    {
        List<XtpGameInfo> list = xtpGameInfoService.selectXtpGameInfoList(xtpGameInfo);
        ExcelUtil<XtpGameInfo> util = new ExcelUtil<XtpGameInfo>(XtpGameInfo.class);
        util.exportExcel(response, list, "策略信息数据");
    }

    /**
     * 获取策略信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpGameInfo:query')")
    @GetMapping(value = "/{gameId}")
    public AjaxResult getInfo(@PathVariable("gameId") Long gameId)
    {
        return success(xtpGameInfoService.selectXtpGameInfoByGameId(gameId));
    }

    /**
     * 新增策略信息
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpGameInfo:add')")
    @Log(title = "策略信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody XtpGameInfo xtpGameInfo)
    {
        return toAjax(xtpGameInfoService.insertXtpGameInfo(xtpGameInfo));
    }

    /**
     * 修改策略信息
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpGameInfo:edit')")
    @Log(title = "策略信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody XtpGameInfo xtpGameInfo)
    {
        return toAjax(xtpGameInfoService.updateXtpGameInfo(xtpGameInfo));
    }

    /**
     * 删除策略信息
     */
    @PreAuthorize("@ss.hasPermi('xtp:xtpGameInfo:remove')")
    @Log(title = "策略信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{gameIds}")
    public AjaxResult remove(@PathVariable Long[] gameIds)
    {
        return toAjax(xtpGameInfoService.deleteXtpGameInfoByGameIds(gameIds));
    }
}
