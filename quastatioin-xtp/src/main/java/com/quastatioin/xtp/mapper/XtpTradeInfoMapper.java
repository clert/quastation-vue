package com.quastatioin.xtp.mapper;

import java.util.List;
import com.quastatioin.xtp.domain.XtpTradeInfo;

/**
 * 交易记录Mapper接口
 *
 * @author CCCLERT
 * @date 2024-07-09
 */
public interface XtpTradeInfoMapper
{
    /**
     * 查询交易记录
     *
     * @param tradeId 交易记录主键
     * @return 交易记录
     */
    public XtpTradeInfo selectXtpTradeInfoByTradeId(Long tradeId);

    /**
     * 查询交易记录列表
     *
     * @param xtpTradeInfo 交易记录
     * @return 交易记录集合
     */
    public List<XtpTradeInfo> selectXtpTradeInfoList(XtpTradeInfo xtpTradeInfo);

    /**
     * 新增交易记录
     *
     * @param xtpTradeInfo 交易记录
     * @return 结果
     */
    public int insertXtpTradeInfo(XtpTradeInfo xtpTradeInfo);

    /**
     * 修改交易记录
     *
     * @param xtpTradeInfo 交易记录
     * @return 结果
     */
    public int updateXtpTradeInfo(XtpTradeInfo xtpTradeInfo);

    /**
     * 删除交易记录
     *
     * @param tradeId 交易记录主键
     * @return 结果
     */
    public int deleteXtpTradeInfoByTradeId(Long tradeId);

    /**
     * 批量删除交易记录
     *
     * @param tradeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteXtpTradeInfoByTradeIds(Long[] tradeIds);
}
