package com.quastatioin.iot.controller;

import com.quastatioin.common.core.controller.BaseController;
import com.quastatioin.iot.mqtt.EmqxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Xtp Controller 需要资金信息Controller
 *
 * @author CCLERT
 * @date 2024-07-09
 */
@RestController
@RequestMapping("/xtp/xtpGameInfo")
public class XtpController extends BaseController
{

    @Lazy
    @Autowired
    private EmqxService emqxService;
}
