package com.quastatioin.iot.service;

import com.quastatioin.common.core.domain.entity.SysUser;
import com.quastatioin.iot.model.MqttAuthenticationModel;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * 
 * @author kerwincui
 * @date 2021-12-16
 */
public interface IToolService
{

    /**
     * 生成随机数字和字母
     */
    public String getStringRandom(int length);

    /**
     * 返回认证信息
     */
    public ResponseEntity returnUnauthorized(MqttAuthenticationModel mqttModel, String message);
}
