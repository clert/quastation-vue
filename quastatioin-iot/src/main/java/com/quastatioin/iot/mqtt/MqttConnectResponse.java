package com.quastatioin.iot.mqtt;

public class MqttConnectResponse {

    private String result;

    private boolean is_superuser;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isIs_superuser() {
        return is_superuser;
    }

    public void setIs_superuser(boolean is_superuser) {
        this.is_superuser = is_superuser;
    }

    public MqttConnectResponse(String result, boolean is_superuser) {
        this.result = result;
        this.is_superuser = is_superuser;
    }


    public MqttConnectResponse(boolean ok) {
        this.result = ok ? "allow":"deny";
        this.is_superuser = false;
    }
}
