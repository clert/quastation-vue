package com.quastatioin.iot.mqtt;

import com.quastatioin.grpc.service.IXtpFundService;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmqxService {
    private static final Logger logger = LoggerFactory.getLogger(EmqxService.class);

    @Autowired
    private EmqxClient emqxClient;

    @Autowired
    private IXtpFundService xtpFundService;

    /**
     * 订阅的主题
     */
    private static final String prefix = "/+/+/";
    String sInfoTopic = prefix + "info/post";
    String sNtpTopic = prefix + "ntp/post";
    String sPropertyTopic = prefix + "property/post";
    String sFunctionTopic = prefix + "function/post";
    String sEventTopic = prefix + "event/post";
    String sShadowPropertyTopic = prefix + "property-offline/post";
    String sShadowFunctionTopic = prefix + "function-offline/post";

    /**
     * 发布的主题
     */
    String pStatusTopic = "/status/post";
    String pInfoTopic = "/info/get";
    String pNtpTopic = "/ntp/get";
    String pPropertyTopic = "/property/get";
    String pFunctionTopic = "/function/get";

    public void subscribe(MqttAsyncClient client) throws MqttException {
//        // 订阅设备信息
//        client.subscribe(sInfoTopic, 1);
//        // 订阅时钟同步
//        client.subscribe(sNtpTopic, 1);
//        // 订阅设备属性
//        client.subscribe(sPropertyTopic, 1);
//        // 订阅设备功能
//        client.subscribe(sFunctionTopic, 1);
//        // 订阅设备事件
//        client.subscribe(sEventTopic, 1);
//        // 订阅属性（影子模式）
//        client.subscribe(sShadowPropertyTopic, 1);
//        // 订阅功能（影子模式）
//        client.subscribe(sShadowFunctionTopic, 1);
        logger.info("mqtt订阅了设备信息和物模型主题");
    }

    /**
     * 消息回调方法
     * @param topic  主题
     * @param mqttMessage 消息体
     */
    @Async
    public void subscribeCallback(String topic, MqttMessage mqttMessage) throws InterruptedException {

        /**测试线程池使用*/
        logger.info("====>>>>线程名--{}",Thread.currentThread().getName());
        /**模拟耗时操作*/
        // Thread.sleep(1000);
        // subscribe后得到的消息会执行到这里面
        String message = new String(mqttMessage.getPayload());
        logger.info("接收消息主题 : " + topic);
        logger.info("接收消息Qos : " + mqttMessage.getQos());
        logger.info("接收消息内容 : " + message);

        String[] topicItem = topic.substring(1).split("/");
        Long productId = Long.valueOf(topicItem[0]);
        String deviceNum = topicItem[1];
        String name = topicItem[2];
        switch (name) {
            case "info":
//                reportDevice(productId, deviceNum, message);
                break;
        }
    }


    /**
     * 1.发布设备状态
     */
    public void publishStatus(Long productId, String deviceNum, int deviceStatus, int isShadow,int rssi) {
        String message = "{\"status\":" + deviceStatus + ",\"isShadow\":" + isShadow + ",\"rssi\":" + rssi + "}";
        emqxClient.publish(1, false, "/" + productId + "/" + deviceNum + pStatusTopic, message);
    }

    /**
     * 2.发布设备信息
     */
    public void publishInfo(Long productId, String deviceNum) {
        emqxClient.publish(1, false, "/" + productId + "/" + deviceNum + pInfoTopic, "");
    }

    /**
     * 3.获取资金账户数据
     */
    public void publishXtpFundInfo() {
        xtpFundService.getXtpFundInfo("fundInfo");
    }

    /**
     * 3.获取资金账户数据
     */
    public void publishXtpPositionInfo() {
        xtpFundService.getXtpFundInfo("positionInfo");
    }

}
