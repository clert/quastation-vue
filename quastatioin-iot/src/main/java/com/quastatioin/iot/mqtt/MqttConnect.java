package com.quastatioin.iot.mqtt;

public class MqttConnect {

    private String username;

    private String peerhost;

    private String password;

    private String clientid;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPeerhost() {
        return peerhost;
    }

    public void setPeerhost(String peerhost) {
        this.peerhost = peerhost;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }
}
