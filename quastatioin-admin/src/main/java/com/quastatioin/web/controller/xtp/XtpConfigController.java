package com.quastatioin.web.controller.xtp;

import com.quastatioin.common.annotation.Log;
import com.quastatioin.common.core.controller.BaseController;
import com.quastatioin.common.core.domain.AjaxResult;
import com.quastatioin.common.enums.BusinessType;
import com.quastatioin.common.utils.ProcessUtil;
import com.quastatioin.system.domain.XtpConfig;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/system/xtpjson")
public class XtpConfigController extends BaseController {


    /**
     * 获取配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:xtpjson:list')")
    @GetMapping("/list")
    public AjaxResult list() throws IOException {

        XtpConfig xtpConfig = new XtpConfig("config.json", ProcessUtil.readFileToJson(1));
        XtpConfig xtpConfig_1 = new XtpConfig("application.json", ProcessUtil.readFileToJson(2));

        List<XtpConfig> configList = new ArrayList<XtpConfig>();
        configList.add(xtpConfig);configList.add(xtpConfig_1);

        return success(configList);
    }


    /**
     * 修改配置
     */
    @PreAuthorize("@ss.hasPermi('system:xtpjson:edit')")
    @Log(title = "配置管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody  XtpConfig xtpConfig)
    {
        try {
            ProcessUtil.writeJsonFile(xtpConfig.getConfigName(),xtpConfig.getConfigValue());
            return AjaxResult.success();
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }

}
