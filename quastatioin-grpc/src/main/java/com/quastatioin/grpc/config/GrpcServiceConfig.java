package com.quastatioin.grpc.config;

import com.quastatioin.grpc.xproto.GreeterGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 微信公众号：微观技术
 */

@Configuration
@ConfigurationProperties(prefix = "xgrpc")
public class GrpcServiceConfig {

    /** 项目名称 */
    private String server;

    /** 版本 */
    private Integer port;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Bean
    public ManagedChannel getChannel() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(server, port)
                .usePlaintext()
                .build();
        return channel;
    }

    @Bean
    public GreeterGrpc.GreeterBlockingStub getStub(ManagedChannel channel) {
        return GreeterGrpc.newBlockingStub(channel);
    }

}