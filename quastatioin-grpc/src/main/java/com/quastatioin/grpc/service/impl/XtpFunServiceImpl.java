package com.quastatioin.grpc.service.impl;

import com.quastatioin.grpc.service.IXtpFundService;
import com.quastatioin.grpc.xproto.GreeterGrpc;
import com.quastatioin.grpc.xproto.HelloReply;
import com.quastatioin.grpc.xproto.HelloRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class XtpFunServiceImpl implements IXtpFundService {


    private static final Logger logger = LoggerFactory.getLogger(XtpFunServiceImpl.class);

    @Resource
    GreeterGrpc.GreeterBlockingStub helloServiceBlockingStub;

    /**
     * 获取资金账户数据
     * @param xtpUserInfo 用户信息
     * @return
     */
    @Override
    public HelloReply getXtpFundInfo(String xtpUserInfo)
    {
        HelloRequest request = HelloRequest.newBuilder()
                .setName(xtpUserInfo)
                .build();
        HelloReply helloResponse = helloServiceBlockingStub.sayHello(request);
        String result = helloResponse.getMessage();

        logger.info("获取资金账户数据 : " + result);
        return helloResponse;
    }



    /**
     * 获取用户持仓数据
     * @param xtpUserInfo 用户信息
     * @return
     */
    public HelloReply getXtpPositionInfo(String xtpUserInfo)
    {

        HelloRequest request = HelloRequest.newBuilder()
                .setName(xtpUserInfo)
                .build();
        HelloReply helloResponse = helloServiceBlockingStub.sayHello(request);
        String result = helloResponse.getMessage();

        logger.info("获取资金账户数据 : " + result);
        return helloResponse;
    }
}
