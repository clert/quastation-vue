package com.quastatioin.grpc.service;

import com.quastatioin.grpc.xproto.HelloReply;

public interface IXtpFundService {

    /**
     * 获取资金账户数据
     * @param xtpUserInfo 用户信息
     * @return
     */
    public HelloReply getXtpFundInfo(String xtpUserInfo);


    /**
     * 获取用户持仓数据
     * @param xtpUserInfo 用户信息
     * @return
     */
    public HelloReply getXtpPositionInfo(String xtpUserInfo);
}
