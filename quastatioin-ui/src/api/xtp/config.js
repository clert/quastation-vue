import request from '@/utils/request'

// 查询配置列表
export function listConfig(query) {
  return request({
    url: '/system/xtpjson/list',
    method: 'get',
    params: query
  })
}

// 修改配置
export function updateConfig(data) {
  return request({
    url: '/system/xtpjson',
    method: 'put',
    data: data
  })
}