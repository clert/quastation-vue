import request from '@/utils/request'

// 查询策略信息列表
export function listXtpGameInfo(query) {
  return request({
    url: '/xtp/xtpGameInfo/list',
    method: 'get',
    params: query
  })
}

// 查询策略信息详细
export function getXtpGameInfo(gameId) {
  return request({
    url: '/xtp/xtpGameInfo/' + gameId,
    method: 'get'
  })
}

// 新增策略信息
export function addXtpGameInfo(data) {
  return request({
    url: '/xtp/xtpGameInfo',
    method: 'post',
    data: data
  })
}

// 修改策略信息
export function updateXtpGameInfo(data) {
  return request({
    url: '/xtp/xtpGameInfo',
    method: 'put',
    data: data
  })
}

// 删除策略信息
export function delXtpGameInfo(gameId) {
  return request({
    url: '/xtp/xtpGameInfo/' + gameId,
    method: 'delete'
  })
}
