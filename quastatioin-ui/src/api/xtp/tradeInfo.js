import request from '@/utils/request'

// 查询交易记录列表
export function listXtpTradeInfo(query) {
  return request({
    url: '/xtp/xtpTradeInfo/list',
    method: 'get',
    params: query
  })
}

// 查询交易记录详细
export function getXtpTradeInfo(tradeId) {
  return request({
    url: '/xtp/xtpTradeInfo/' + tradeId,
    method: 'get'
  })
}

// 新增交易记录
export function addXtpTradeInfo(data) {
  return request({
    url: '/xtp/xtpTradeInfo',
    method: 'post',
    data: data
  })
}

// 修改交易记录
export function updateXtpTradeInfo(data) {
  return request({
    url: '/xtp/xtpTradeInfo',
    method: 'put',
    data: data
  })
}

// 删除交易记录
export function delXtpTradeInfo(tradeId) {
  return request({
    url: '/xtp/xtpTradeInfo/' + tradeId,
    method: 'delete'
  })
}
