const state = {
    // 资金数据
    fundInfo: undefined,
    // 持仓数据
    posInfo: undefined
  }
const mutations = {
    SET_FUND_INFO: (state, data) => {
        state.fundInfo = data
    },
    
    SET_POSITION_INFO: (state, data) => {
        state.posInfo = data
    }
}

const actions = {
    // 1.设置资金信息
    setFundInfo({ commit }, data) {
        commit('SET_FUND_INFO', data)
    },
    // 2.设置持仓信息
    setPositionInfo({ commit }, data) {
        commit('SET_POSITION_INFO', data)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  
  