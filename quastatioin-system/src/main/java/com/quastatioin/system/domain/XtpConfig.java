package com.quastatioin.system.domain;

public class XtpConfig {

    private String configName;

    private String configValue;

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public XtpConfig(String configName, String configValue) {
        this.configName = configName;
        this.configValue = configValue;
    }

    public XtpConfig() {
    }

}
