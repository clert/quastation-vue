package com.quastatioin.quartz.task;

import com.quastatioin.iot.mqtt.EmqxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import com.quastatioin.common.utils.StringUtils;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{
    @Lazy
    @Autowired
    private EmqxService emqxService;

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    // 1.定时查询资金信息、持仓信息
    public void ryNoParams()
    {

        // 1.获取资金数据
        emqxService.publishXtpFundInfo();

        // 2.获取持仓信息
        emqxService.publishXtpPositionInfo();
    }

    // 2.定时同步交易数据

}
