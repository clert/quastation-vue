package com.quastatioin.common.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.quastatioin.common.utils.file.FileUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * 命令行工具
 */
public class ProcessUtil {
    /**
     * 执行命令
     * @param command
     * @return
     * @throws IOException
     */
    public static Process performCommand(String command) throws IOException{
        //command：命令
        return Runtime.getRuntime().exec(command);
    }

    public static String readFileToJson(int fileType) throws UnsupportedEncodingException, IOException {
        JSONObject jsonObject = new JSONObject();

        String filePathUrl = fileType == 1? "D:\\QuaTools\\XTPNew\\build\\x64\\Release\\config.json" : "D:\\QuaTools\\XTPNew\\build\\x64\\Release\\application.json";
        File file = new File(filePathUrl);
        String jsonStr ="";
        Reader reader = new InputStreamReader(new FileInputStream(file),"utf-8");
        int ch = 0;
        StringBuffer sb = new StringBuffer();
        while ((ch = reader.read()) != -1) {
            sb.append((char) ch);
        }
        reader.close();
        jsonStr = sb.toString();

        return jsonStr;
    }

    /**
     * 将Root类型实体写成Json文件
     *
     * @param fileName Root实体
     * @param fileValue     Json文件路径
     */
    public static void writeJsonFile(String fileName, String fileValue) throws IOException {
        String filePathUrl = "config.json".equals(fileName)? "D:\\QuaTools\\XTPNew\\build\\x64\\Release\\config.json" : "D:\\QuaTools\\XTPNew\\build\\x64\\Release\\application.json";
        FileUtils.writeJsonBytes(fileValue.getBytes(StandardCharsets.UTF_8),filePathUrl);
    }


    /**
     * 关闭开启程序
     * @param isOpen
     * @param applicationName
     * @return
     */
    public static void performCommand(boolean isOpen, String applicationName) throws IOException
    {
        String command;

        if(isOpen) {
            command = "d\\decodeServer\\DecodingOnWallService.exe";
        }
        else {
            command = "taskkill /f /im " + applicationName + ".exe";
        }
        Process pro = performCommand(command);
    }
}
